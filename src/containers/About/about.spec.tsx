import React from 'react'
import { render } from '@testing-library/react'

import About from './about'

describe('About Page', () =>{
    it('renders correctly', ()=>{
        const {asFragment} = render(<About />)
        expect(asFragment()).toMatchSnapshot()
    })

    it('renders hello', () => {
        const EXPECTED = `Hello from ${process.env.SITE_NAME}.`

        const { getByTestId } = render(<About />)
    
        // When
        const ACTUAL = getByTestId('helloH1').textContent
    
        // Then
        expect(ACTUAL).toEqual(EXPECTED)
      })
    
      it('renders hello with tailwind classes', () => {
        // Given
        const EXPECTED = 'text-xl text-gray-900'
        const { getByTestId } = render(<About />)
    
        // When
        const ACTUAL = getByTestId('helloH1').className
    
        // Then
        expect(ACTUAL).toContain(EXPECTED)
      })
})