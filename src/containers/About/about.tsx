import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout' 

const About: React.FC = () => {
    const { SITE_NAME } = process.env

    return(
        <div>
            <Layout title=" ArtGlud 🎨 | About">

                <h1 className="">
                    Sobre Nosotros - Art Glud 
                </h1>

                <p>
                    increible, no lo puedo creer
                </p>

                <p>
                    <Link href="/">
                        <a> Go Home </a>
                    </Link>
                </p>

            </Layout>
        </div>
    )
}

export default About