import React from 'react'
import Link from 'next/link'
import Layout from '../../components/Layout'

const Home: React.FC = () => {
  const { SITE_NAME } = process.env

  return (
    <body className="text-gray-700 bg-white">
      <Layout title=" ArtGlud 🎨 | Home">
      <div className="flex">
      </div>
      <div className=".justify-center">
        <h1 className="text-6xl text-center">{SITE_NAME}! 🎨</h1>
        
        
        <h1 data-testid="helloH1" className="text-xl text-gray-900 text-center">
          Bienvenidos a {SITE_NAME}.
        </h1>
        
        <p>
          <Link href="/about">
            <a>About</a>
          </Link>
        </p>
      </div> 
      </Layout>
    </body>
  )
}

export default Home
